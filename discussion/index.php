<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Repetition Control Structure and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>

	<h2>While Loop</h2>
	<p><?php whileLoop(); ?></p>

	<h2>Do While Loop</h2>
	<p><?php doWhileLoop(); ?></p>

	<h2>For Loop</h2>
	<p><?php forLoop(); ?></p>

	<h2>Continue and Break</h2>
	<p><?php modifiedForLoop(); ?></p>

	<h1>Array Manipulation</h1>

	<h2>Types of Array</h2>

	<h3>Simple Array</h3>
	<!-- 
		foreach
			-this loop only works on arrays
			-loop through each key/value pair in an array
		Syntax:
			foreach($array as $element){
				//code block to be executed
			}
	 -->
	<ul>
		<?php foreach($computerBrands as $brand){?>
			<li><?= $brand ?></li>
		<?php } ?>
	</ul>


	<h3>Associative Array</h3>
	<ul>
		<?php foreach($gradesPeriods as $period => $grade){?>
			<li>Grade in <?=$period?> is <?=$grade?></li>
		<?php } ?>
	</ul>


	<h3>Multidimensional Array</h3>
	<ul>
		<?php 
			foreach($heroes as $team){
				foreach($team as $member){?>
					<li><?= $member?></li>
				<?php }} ?>

	</ul>

	<h3>Two Dimensional Associative Array</h3>

	<ul>
		<?php 
			foreach($ironManPowers as $powerType => $value){
				foreach($value as $power){?>
					<li><?=$powerType?>: <?=$power?></li>
				<?php }} ?>

	</ul>


	<h2>Array Functions</h2>

	<h3>Original Array</h3>
	<pre><?php print_r($computerBrands)?></pre>

	<h3>Sorted Array</h3>
	<h4>Sorting in Ascending Order</h4>
	<pre><?php print_r($sortedBrands)?></pre>
	<h4>Sorting in Descending Order</h4>
	<pre><?php print_r($reversedSortedBrands)?></pre>

	<h3>Append</h3>

	<h4>Add one or more element on the end of an array</h4>
	<?php array_push($computerBrands, "Apple");?>
	<pre><?php print_r($computerBrands);?></pre>

	<h4>Add one or more element on the end of an array</h4>
	<?php array_unshift($computerBrands, "Dell");?>
	<pre><?php print_r($computerBrands);?></pre>

	<h3>Removed</h3>

	<h4>Removed the element off in the end of an array</h4>
	<?php array_pop($computerBrands);?>
	<pre><?php print_r($computerBrands);?></pre>

	<h4>Removed the element off in the end of an array</h4>
	<?php array_shift($computerBrands);?>
	<pre><?php print_r($computerBrands);?></pre>


	<h3>Others</h3>
	<h4>Count the number of elements in array</h4>
	<pre><?php echo count($computerBrands)?></pre>

	<h4>in_array(): is used to check if the element exists in the array</h4>
	<pre><?php echo searchBrand($computerBrands, "HP")?></pre>
	<pre><?php echo searchBrand($computerBrands, "Neo")?></pre>

	<h4>array_reverse: returns the array in the reverse order</h4>
	<pre><?php print_r($gradesPeriods); ?></pre>
	<pre><?php print_r($reversedGradePeriods); ?></pre>
</body>
</html>