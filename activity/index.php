<?php require_once "./code.php";?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Activity</title>
</head>
<body>
	<h2>Divisible by Five</h2>
	<p> <?php printDivisibleOfFive(); ?> </p>


	<h2>Array Manipulation</h2>

	<?php array_push($students, "John Smith") ?>
	<p><?php var_dump($students) ?></p>
	<p><?= count($students) ?></p>

	<?php array_push($students, "Jane Smith")?>
	<p><?php var_dump($students) ?></p>
	<p><?= count($students) ?></p>

	<?php array_shift($students)?>
	<p><?php var_dump($students) ?></p>
	<p><?= count($students) ?></p>
</body>
</html>